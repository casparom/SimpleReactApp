import * as actionTypes from "./../actions/actionTypes";

const initialState = {
    changingId: "",
    people: {}
};

const peoplePage = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_PEOPLE:
            return {
                ...state,
                people: {...action.payload.reduce((result = {}, person) => {
                        result[person.id] = person;
                        return result;
                    }, state.people)}
            };
        case actionTypes.REMOVE_PERSON:
            return Object.assign({}, state, {
                people: Object.keys(state.people).reduce((result, key) => {
                    if (key.toString() !== action.payload.id.toString()) {
                        result[key] = state.people[key];
                    }
                    return result;
                }, {})
            });
        case actionTypes.CHANGE_PERSON:
            return {
                ...state,
                people: {
                    ...state.people,
                    [action.payload.id]: action.payload
                }
            };
        case actionTypes.SET_CHANGING_ID:
            return {
                ...state,
                changingId: action.payload
            };
        default:
            return state;
    }
}

export default peoplePage;