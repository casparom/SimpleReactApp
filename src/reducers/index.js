import {combineReducers} from 'redux';
import peoplePage from './PeoplePageReducer';

const rootReducer = combineReducers({
    peoplePage,
});

export default rootReducer;