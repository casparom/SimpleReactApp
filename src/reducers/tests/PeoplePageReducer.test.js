import reducer from './../PeoplePageReducer';
import * as actions from './../../actions/actionTypes';
import chai from 'chai';

const expect = chai.expect;

function createName(number) {
    return {id: number, name: "name" + number.toString(), surname: "surname" + number.toString()};
}

describe("reducer tests", () => {

    it("should return initial state", () => {
        let expected = {changingId: "", people:{}};
        expect(JSON.stringify(reducer(undefined, {}))).to.deep.equal(JSON.stringify(expected));
    });

    it("should add a name to the empty store when addNames action dispatched", () => {
        let expected = {changingId: "", people:{0: createName(0)}};
        expect(JSON.stringify(reducer(undefined, {
            type: actions.ADD_PEOPLE,
            payload: [createName(0)]
        }))).to.deep.equal(JSON.stringify(expected));
    });

    it("should add a name to the non-empty store when addNames action dispatched", () => {
        let initial = {people:{0: createName(0)}};
        let expected = {people:{0: createName(0), 1: createName(1)}};
        expect(JSON.stringify(reducer(initial, {
            type: actions.ADD_PEOPLE,
            payload: [createName(1)]
        }))).to.deep.equal(JSON.stringify(expected));
    });

    it("should add multiple names when addNames action dispatched", () => {
        let initial = {people:{}};
        let expected = {people:{0: createName(0), 1: createName(1)}};
        expect(JSON.stringify(reducer(initial, {
            type: actions.ADD_PEOPLE,
            payload: [createName(0), createName(1)]
        }))).to.deep.equal(JSON.stringify(expected));
    });

    it("should remove the correct name when removeName action dispatched", () => {
        let initial = {people:{0: createName(0), 1: createName(1)}};
        let expected = {people:{0: createName(0)}};
        expect(JSON.stringify(reducer(initial, {
            type: actions.REMOVE_PERSON,
            payload: createName(1)
        }))).to.deep.equal(JSON.stringify(expected));
    });

    it("should change the correct name when changeName action dispatched", () => {
        let changedName = {...createName(0), name: "changedName"};
        let initial = {people:{0: createName(0), 1: createName(1)}};
        let expected = {people:{0: changedName, 1: createName(1)}};
        expect(JSON.stringify(reducer(initial, {
            type: actions.CHANGE_PERSON,
            payload: changedName
        }))).to.deep.equal(JSON.stringify(expected));
    });

    it("should change the changingId when setChangingId action dispatched", () => {
        let initial = {changingId: -1, people:{}};
        let expected = {changingId: 3, people:{}};
        expect(JSON.stringify(reducer(initial, {
            type: actions.SET_CHANGING_ID,
            payload: 3
        }))).to.deep.equal(JSON.stringify(expected));
    });

});