import * as redux from "redux";
import rootReducer from "../reducers/index";
import thunk from 'redux-thunk';

const store = redux.createStore(rootReducer, redux.applyMiddleware(thunk));

export default store;