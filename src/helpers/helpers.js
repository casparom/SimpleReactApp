export function createId(item) {
    return Object.keys(item).reduce((result, key) => {
        result += item[key];
        return result;
    }, "");
}