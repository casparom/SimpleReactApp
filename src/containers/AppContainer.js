import App from './../components/App';
import * as react_redux from 'react-redux';
import { withRouter } from 'react-router-dom';

const mapStateToProps = (state) => {
    return {
        state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const AppContainer = withRouter(react_redux.connect(
    mapStateToProps,
    mapDispatchToProps
)(App));

export default AppContainer
