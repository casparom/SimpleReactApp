import NameRow from './../components/NameRow';
import * as react_redux from 'react-redux';
import * as actions from './../actions/peopleActions';
import { withRouter } from 'react-router-dom'

const mapStateToProps = (state) => {
    return {
        ...state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeClicked: (payload) => {
            dispatch(actions.setChangingId(payload.id));
        },

        handleOkClicked: (payload) => {
            dispatch(actions.setChangingId(""));
            dispatch(actions.changePerson(payload));
        },

        handleCancelClicked: (payload) => {
            dispatch(actions.setChangingId(""));
        },

        handleDeleteClicked: (payload) => {
            dispatch(actions.removePerson(payload));
        }
    }
};

const NameRowContainer = withRouter(react_redux.connect(
    mapStateToProps,
    mapDispatchToProps
)(NameRow));

export default NameRowContainer
