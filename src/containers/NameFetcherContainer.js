import NameFetcher from './../components/NameFetcher';
import * as react_redux from 'react-redux';
import * as actions from './../actions/peopleActions';
import { withRouter } from 'react-router-dom';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleFetchClicked: (payload) => {
            dispatch(actions.fetchPeople(payload));
        }
    }
};

const NameFetcherContainer = withRouter(react_redux.connect(
    mapStateToProps,
    mapDispatchToProps
)(NameFetcher));

export default NameFetcherContainer
