import PeoplePage from './../components/PeoplePage';
import * as react_redux from 'react-redux';
import { withRouter } from 'react-router-dom';

const mapStateToProps = (state) => {
    return {
        ...state.peoplePage
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const PeoplePageContainer = withRouter(react_redux.connect(
    mapStateToProps,
    mapDispatchToProps
)(PeoplePage));

export default PeoplePageContainer
