import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Form, ControlLabel, Button} from 'react-bootstrap';
import './../static/NameFetcher.css';

class NameFetcher extends Component {
    constructor(props) {
        super(props);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleCountChange = this.handleCountChange.bind(this);
        this.handleFetchClicked = this.handleFetchClicked.bind(this);

        this.state = {
            show: false,
            count: 1
        }
    }

    handleClose() {
        this.setState({...this.state, show: false});
    }

    handleShow() {
        this.setState({...this.state, show: true});
    }

    handleCountChange(event) {
        this.setState({...this.state, count: event.target.value});
    }

    handleFetchClicked() {
        this.props.handleFetchClicked(this.state.count);
    }

    render() {
        return (
            <div className="nameFetcher">
                <Form inline>
                        <ControlLabel className="nameFetcherElement">Fetch</ControlLabel>
                        <input id="fetchInput" type="number" name="countInput" onChange={this.handleCountChange} className="nameFetcherElement"/>
                        <ControlLabel className="nameFetcherElement">names</ControlLabel>
                        <Button id="fetchButton" onClick={this.handleFetchClicked} className="nameFetcherElement" bsSize="small" bsStyle="info">Fetch</Button>
                </Form>
            </div>
        );
    }
}

NameFetcher.propTypes = {
    handleFetchClicked: PropTypes.func.isRequired
};

export default NameFetcher;
