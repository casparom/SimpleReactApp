import React from 'react';
import PropTypes from 'prop-types';
import Enzyme, {mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PeoplePage from './../PeoplePage';
import chai from 'chai';

const expect = chai.expect;

Enzyme.configure({adapter: new Adapter()});

let props = {
    changingId: "",
    people: {}
};

function resetProps() {
    props.changingId = "";
    props.people = {};
};

PeoplePage.propTypes = {
    changingId: PropTypes.string.isRequired,
    people: PropTypes.object.isRequired
};

function setupWrapper() {
    return shallow(<PeoplePage {...props} />);
}

describe("peoplePage tests", () => {

    let wrapper = null;

    beforeEach(() => {
        wrapper = setupWrapper();
    });

    afterEach(() => {
        resetProps();
    });

    it('should have valid props', () => {
        expect(wrapper.instance().props).to.deep.equal(props);
    });

    it('should have div container as the first element', () => {
        expect(wrapper.first().type()).to.equal("div");
        expect(wrapper.first().hasClass("peoplePage")).to.equal(true);
    });

    it('should have page title and page contents', () => {
        expect(wrapper.first().children().length).to.equal(2);
        let title = wrapper.first().childAt(0);
        expect(title.type()).to.equal("h4");
        expect(title.hasClass("pageTitle")).to.equal(true);
        let contents = wrapper.first().childAt(1);
        expect(contents.type()).to.equal("div");
        expect(contents.hasClass("pageContents")).to.equal(true);
    });

    it('should have nameFetcher and nameTable', () => {
        expect(wrapper.find("#nameFetcher").length).to.equal(1);
        expect(wrapper.find("#nameTable").length).to.equal(1);
    });

    it('should pass props to nameTable', () => {
        expect(wrapper.find("#nameTable").props().children.props).to.deep.equal(props);
    });

});
