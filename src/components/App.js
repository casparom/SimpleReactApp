import React, { Component } from 'react';
import './../static/App.css';
import {Route, Link, Switch} from 'react-router-dom';
import PeoplePageContainer from './../containers/PeoplePageContainer';
import AboutPage from './AboutPage';

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="AppHeader">
                    <h1>React Demo</h1>
                </header>
                <div className="AppMain">
                    <Switch>
                        <Route exact path="/" component={PeoplePageContainer}/>
                        <Route exact path="/about" component={AboutPage}/>
                    </Switch>
                </div>
                <footer className="AppFooter">
                    <ul>
                        <h3 id="navigateTitle">Pages</h3>{" "}
                        <Link to="/" className="link">Names</Link>
                        <Link to="/about" className="link">About</Link>
                    </ul>
                </footer>
            </div>
        );
    }
}

export default App;
