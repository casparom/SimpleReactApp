import React, { Component } from 'react';
import './../static/AboutPage.css';

class AboutPage extends Component {
    render() {
        return (
            <div className="AboutPage">
                <h4 className="pageTitle">About</h4>
                <div className="pageContents">
                    <p>
                        This is a demo React project
                    </p>
                </div>
            </div>
        );
    }
}

export default AboutPage;
