import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NameTable from './NameTable';
import NameFetcherContainer from './../containers/NameFetcherContainer';
import './../static/PeoplePage.css';

class PeoplePage extends Component {
    render() {
        return (
            <div className="peoplePage">
                <h4 className="pageTitle">People</h4>
                <div className="pageContents">
                    <div id="nameFetcher">
                        <NameFetcherContainer/>
                    </div>
                    <div id="nameTable">
                        <NameTable {...this.props}/>
                    </div>
                </div>
            </div>
        );
    }
}

PeoplePage.propTypes = {
    people: PropTypes.object.isRequired,
    changingId: PropTypes.string.isRequired
};

export default PeoplePage;
