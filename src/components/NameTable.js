import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {ListGroup} from 'react-bootstrap';
import NameRowContainer from './../containers/NameRowContainer';
import './../static/NameTable.css';

class NameTable extends Component {

    setUpNames(people) {
        let rows = [];
        for(let i in people) {
            let row = <NameRowContainer key={i} {
                ...{person: people[i],
                    isChanging: this.props.changingId.toString() === people[i].id.toString()
                   }
            }/>;
            rows.push(row);
        }
        return rows;
    }

    render() {
        return (
            <ListGroup className="Name-table" componentClass="ul">
                {this.setUpNames(this.props.people)}
            </ListGroup>
        );
    }
}

NameTable.propTypes = {
    people: PropTypes.object.isRequired,
    changingId: PropTypes.string.isRequired
};

export default NameTable;
