import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Form, FormControl, Button} from 'react-bootstrap';
import './../static/NameRow.css';

class NameRow extends Component {
    constructor(props) {
        super(props);

        this.handleChangeClicked = this.handleChangeClicked.bind(this);
        this.handleOkClicked = this.handleOkClicked.bind(this);
        this.handleCancelClicked = this.handleCancelClicked.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSurnameChange = this.handleSurnameChange.bind(this);
        this.handleDeleteClicked = this.handleDeleteClicked.bind(this);

        this.state = {
            name: this.props.person.name,
            surname: this.props.person.surname,
        }
    }

    handleChangeClicked(event) {
        this.props.handleChangeClicked(this.props.person);
    }

    handleOkClicked(event) {
        this.props.handleOkClicked({...this.props.person, name: this.state.name, surname: this.state.surname})
    }

    handleCancelClicked(event) {
        this.setState({...this.state, name: this.props.person.name, surname: this.props.person.surname});
        this.props.handleCancelClicked(this.props.person)
    }

    handleDeleteClicked(event) {
        this.props.handleDeleteClicked(this.props.person)
    }

    handleNameChange(event) {
        this.setState({...this.state, name: event.target.value});
    }

    handleSurnameChange(event) {
        this.setState({...this.state, surname: event.target.value});
    }

    notChangingButtons() {
        return (
            <div className="nameButtons">
                <Button id="changeButton" onClick={this.handleChangeClicked} bsSize="small">Change</Button>
                <Button id="deleteButton" onClick={this.handleDeleteClicked} bsStyle="danger" bsSize="small">Delete</Button>
            </div>
        );
    }

    changingButtons() {
        return (
            <div className="nameButtons">
                <Button id="okButton" onClick={this.handleOkClicked} bsStyle="success" bsSize="small">Ok</Button>
                <Button id="cancelButton" onClick={this.handleCancelClicked} bsSize="small">Cancel</Button>
            </div>
        );
    }

    render() {
        return (
            <div className="nameRow">
                <Form inline>
                    <div className="nameInputs">
                        <FormControl id="nameChanger" type="text" onChange={this.handleNameChange} disabled={!this.props.isChanging} placeholder="Name" value={this.state.name} />
                        <FormControl id="surnameChanger" type="text" onChange={this.handleSurnameChange} disabled={!this.props.isChanging} placeholder="Surname" value={this.state.surname} />
                    </div>
                    {this.props.isChanging ? this.changingButtons(): this.notChangingButtons()}
                </Form>
            </div>
        )
    }
}

NameRow.propTypes = {
    person: PropTypes.object.isRequired,
    isChanging: PropTypes.bool.isRequired,
    handleChangeClicked: PropTypes.func.isRequired,
    handleOkClicked: PropTypes.func.isRequired,
    handleCancelClicked: PropTypes.func.isRequired,
    handleDeleteClicked: PropTypes.func.isRequired

};

export default NameRow;
