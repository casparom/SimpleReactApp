export const ADD_PEOPLE = "ADD_PEOPLE";
export const REMOVE_PERSON = "REMOVE_PERSON";
export const CHANGE_PERSON = "CHANGE_PERSON";
export const SET_CHANGING_ID = "SET_CHANGING_ID";
