import * as actionTypes from './actionTypes';
import 'isomorphic-fetch';
import * as helpers from './../helpers/helpers'

export const addPeople = payload => ({type: actionTypes.ADD_PEOPLE, payload});
export const removePerson = payload => ({type: actionTypes.REMOVE_PERSON, payload});
export const changePerson = payload => ({type: actionTypes.CHANGE_PERSON, payload});
export const setChangingId = payload => ({type: actionTypes.SET_CHANGING_ID, payload});
export const fetchPeople = payload => {
    return function (dispatch) {
        return fetch("https://uinames.com/api/?amount=" + payload).then(response => {
            response.json().then(json => {
                let withIds = [];
                if (payload === 1) {
                    withIds.push(json);
                }
                else {
                    withIds = [...json];
                }
                withIds = withIds.map(item => {
                    item["id"] = helpers.createId(item);
                    return item;
                });
                dispatch(addPeople(withIds));
            }).catch(error => {
                console.log(error);
            })
        }).catch(error => {
            console.log(error);
        });
  };};
