import fetchMock from 'fetch-mock/es5/server';
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import * as types from './../../actions/actionTypes';
import * as peopleActions from './../peopleActions.js';
import chai from 'chai';

const expect = chai.expect;
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

let nameA = {
    "name":"Matthew",
    "surname":"Turner",
    "gender":"male",
    "region":"United States"
};

let nameB = {
    "name":"Sándor",
    "surname":"Ottó",
    "gender":"male",
    "region":"Hungary"
};

describe("peopleActions tests", () => {

    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore();
    });

    it('returns correct type and payload when addPeople is called', () => {
        let action = peopleActions.addPeople('some payload');
        expect(action.type).to.deep.equal(types.ADD_PEOPLE);
        expect(action.payload).to.deep.equal('some payload');
    });

    it('returns correct type and payload when removePerson is called', () => {
        let action = peopleActions.removePerson('some payload');
        expect(action.type).to.deep.equal(types.REMOVE_PERSON);
        expect(action.payload).to.deep.equal('some payload');
    });

    it('returns correct type and payload when changePerson is called', () => {
        let action = peopleActions.changePerson('some payload');
        expect(action.type).to.deep.equal(types.CHANGE_PERSON);
        expect(action.payload).to.deep.equal('some payload');
    });

    it('returns correct type and payload when setChangingId is called', () => {
        let action = peopleActions.setChangingId('some payload');
        expect(action.type).to.deep.equal(types.SET_CHANGING_ID);
        expect(action.payload).to.deep.equal('some payload');
    });

    it('dispatches addPeople with generated ID-s if fetch returns one name', () => {
        fetchMock.getOnce('*', nameA);
        const expected_actions = [
            { payload: [{...nameA, id: "MatthewTurnermaleUnited States"}], type: types.ADD_PEOPLE }
        ];
        const store = mockStore();
        return store.dispatch(peopleActions.fetchPeople(1)).then(() => {
            expect(store.getActions()).to.deep.equal(expected_actions);
        });
    });

    it('dispatches addPeople with generated ID-s if fetch returns multiple names', () => {
        fetchMock.getOnce('*', [nameA, nameB]);
        const expected_actions = [
            { payload: [{...nameA, id: "MatthewTurnermaleUnited States"}, {...nameB, id: "SándorOttómaleHungary"}], type: types.ADD_PEOPLE }
        ];
        const store = mockStore();
        return store.dispatch(peopleActions.fetchPeople(2)).then(() => {
            expect(store.getActions()).to.deep.equal(expected_actions);
        });
    });

});
